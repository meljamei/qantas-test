const mongoose = require('mongoose');

const CamperSchema = new mongoose.Schema({
    name: String,
    points: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Point' }]
}, {timestamps: true})

module.exports = mongoose.model('Camper', CamperSchema)