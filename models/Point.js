const mongoose = require('mongoose');


const PointSchema = new mongoose.Schema({
    amount: Number,
}, {timestamps: true})

module.exports = mongoose.model('Point', PointSchema)