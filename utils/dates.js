const moment = require('moment');

const getPastDay = period => 
    moment(Date.now()).subtract(period, 'days')

const validateCurrentDateIsAfterPeriod = (currentDate, period) => {
    const pastDate = getPastDay(period);
    return moment(currentDate).isAfter(pastDate);
}    

module.exports = {
    getPastDay,
    validateCurrentDateIsAfterPeriod
}