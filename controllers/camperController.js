const Camper = require('../models/Camper');
const { handleCamperAchievments } = require('../helpers');

const listCampers = async (req, res) =>
    await Camper.find({})
        .populate('points')
        .exec(function (err, results) {
            if (err) res.status(500).json(err)
            res.status(200).json(results)
        })

const listCampersByAchievements = async (req, res) => {
    const campers = await Camper.find({}).populate('points').exec()
    const result = handleCamperAchievments(campers, req.query)
    res.status(200).json(result);
}

const createCamper = async (req, res) => {
    return await Camper.create(req.body)
        .then(results => res.status(200).json(results))
        .error(err => res.status(500).json(err))
}

const getCamperDetails = async (req, res) => {
    return await Camper.findById(req.params.id)
        .then(result => res.status(200).json(result))
        .error(err => res.status(500).json(err))
}

const updateCamper = async (req, res) => {
    return await Camper.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(result => res.status(200).json(result))
        .error(err => res.status(500).json(err))
}

const deleteCamper = async (req, res) => {
    return await Camper.findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json({ status: 'Deleted' }))
        .error(err => res.status(500).json(err))
}


module.exports = {
    listCampers,
    createCamper,
    getCamperDetails,
    updateCamper,
    deleteCamper,
    listCampersByAchievements,
};