const Point = require('../models/Point');

const listPoints = async (req, res) => {
    return await Point.find({})
        .then(results => res.status(200).json(results))
        .error(err => res.status(500).json(err))
}

const createPoint = async (req, res) => {
    return await Point.create(req.body)
        .then(results => res.status(200).json(results))
        .error(err => res.status(500).json(err))
}

const getPointDetails = async (req, res) => {
    return await Point.findById(req.params.id)
        .then(result => res.status(200).json(result))
        .error(err => res.status(500).json(err))
}

const updatePoint = async (req, res) => {
    return await Point.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(result => res.status(200).json(result))
        .error(err => res.status(500).json(err))
}

const deletePoint = async (req, res) => {
    return await Point.findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json({ status: 'Deleted'}))
        .error(err => res.status(500).json(err))
}



module.exports = {
    listPoints,
    createPoint,
    getPointDetails,
    updatePoint,
    deletePoint,
};