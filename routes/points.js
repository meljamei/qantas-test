var express = require('express');
var router = express.Router();

const {
    listPoints,
    createPoint,
    getPointDetails,
    updatePoint,
    deletePoint,
} = require("../controllers/pointController");

/* Points APT. */
router.get('/', listPoints);
router.post('/', createPoint);
router.get('/:id', getPointDetails);
router.put('/:id', updatePoint);
router.delete('/:id', deletePoint);

module.exports = router;