var express = require('express');
var router = express.Router();

const {
  listCampers,
  createCamper,
  getCamperDetails,
  updateCamper,
  deleteCamper,
  listCampersByAchievements,
} = require("../controllers/camperController");

/* Campers APT. */
router.get('/', listCampers);
router.post('/', createCamper);
router.get('/:id', getCamperDetails);
router.put('/:id', updateCamper);
router.delete('/:id', deleteCamper);
router.get('/achievments/points', listCampersByAchievements)

module.exports = router;
