# Qantas-test

## Requirements
Build a Camper Leaderboard

Please build a simple web app in Github or something similar

Fulfill the below user stories. Use whichever libraries or APIs you need. Give it your own personal style.
- User Story: I can see a table of the campers who've earned the most brownie points in the past 30 days.
- User Story: I can see how many brownie points they've earned in the past 30 days, and how many they've earned total.
- User Story: I can toggle between sorting the list by how many brownie points they've earned in the past 30 days and by how many brownie points they've earned total.
Hint: To get the top 100 campers for the last 30 days build a simple REST API
Hint: To get the top 100 campers of all time add a new endpoint to the REST API

This is a way to understand your coding style and approach. Don’t spend too much time on the solution. If you are asked to attend an interview, we will review the code with you.


## Solutions
This Project is built in Nodejs & MongoDB + Mongoose

#### Steps

- Created a Camper & a Point Model.
- Each Camper has a number of points
- Created a new route `achievments/points`

#### User Stories

- User Story 1: Campers who've earned the most brownie points in the past 30 days.
Solution: used query strings of filter = period & period = number to achieve the results
Results: /achievments/points/?filter=period&period=30

- User Story 1: I can see how many brownie points they've earned in the past 30 days, and how many they've earned total.
Solution: used a query string as filter = all
Results: /achievments/points/?filter=all

- User Story 3: I can toggle between sorting the list by how many brownie points they've earned in the past 30 days and by how many brownie points they've earned total.
Solution is by changing the value of the filter

### Testing
- There is a Postman collection available for this project

## Technologies
 - Lodash
 - Moment
 - Dotenv
 - Mongoose
 - Jest
