const { validateCurrentDateIsAfterPeriod } = require('../utils/dates')
const _ = require('lodash');

// Filters each users points by date range
// Returns a sorted list of campers by total number of points
const handleCamperAchievments = (campers, query) => {
    const filter = _.get(query, 'filter')
    const period = _.get(query, 'period')
    let results = null;


    switch (filter) {
        case 'period': {
            results = campers.map(camper => {
                return {
                    _id: camper._id,
                    name: camper.name,
                    createdAt: camper.createdAt,
                    updatedAt: camper.updatedAt,
                    points: camper.points.filter(p => validateCurrentDateIsAfterPeriod(p.createdAt, period)),
                    __v: camper.__v,
                }

            }).sort((a, b) => b.points.length - a.points.length)
            break;
        }
        case 'all': {
            results = campers.map(camper => {
                return {
                    _id: camper._id,
                    name: camper.name,
                    createdAt: camper.createdAt,
                    updatedAt: camper.updatedAt,
                    points: camper.points.length,
                    __v: camper.__v,
                }

            }).sort((a, b) => b.points - a.points)
            break;
        }
        default: {
            results = campers;
            break;
        }

    }

    return results;

}


module.exports = {
    handleCamperAchievments
}