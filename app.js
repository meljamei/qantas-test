var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
require('dotenv').config();

var campersRouter = require('./routes/campers');
var pointsRouter = require('./routes/points');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Connect to database
mongoose.set('useCreateIndex', true);

mongoose.connect(process.env.DATABASE_CONNECTION_URL, { useNewUrlParser: true });
mongoose.connection.on("connected", () => {
    console.log('Database connected successfully');
});

mongoose.connection.on("error", err => {
    console.log('Database error' + err );
});

app.use('/campers', campersRouter);
app.use('/points', pointsRouter);

module.exports = app;
